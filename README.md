# Select2 Module

The Select2 module integrates the Select2 library enhancing the functionality
of HTML select elements in Drupal with features like searching, tagging, and
advanced styling options.

## Features

- Seamless integration with the Select2 library.
- Enhanced select elements with features like searching, tagging, and more.
- Improved user experience for selecting options in forms.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

1. Download the module:
   - Visit the [Select2 module page](https://www.drupal.org/project/select2) on Drupal.org.
   - Follow the instructions to download and install the module.

2. Enable the module:
   - Navigate to `Admin > Extend` in your Drupal site.
   - Find the "Select2" module in the list and check the box next to it.
   - Click the "Install" button.

## Usage

Once the module is enabled and configured, the Select2 library will be applied
to relevant select elements in your Drupal site.

## Contributing

We welcome contributions! If you find a bug or have an enhancement in mind, please [create a new issue](https://www.drupal.org/project/issues/select2) on the project's issue queue.

## Maintainers

- [Dave Reid](https://www.drupal.org/u/dave-reid)
- [Adam Delaney](https://www.drupal.org/u/adam-delaney)
- [Owen Bush](https://www.drupal.org/u/owenbush)
- [Adrian Muresan](https://www.drupal.org/u/adrian_s_m)


## License

This module is licensed under the [GNU General Public License, version 2](LICENSE.txt).
